import random

rock = "🪨"
paper = "🧻"
scissors = "✂"

images = [rock, paper, scissors]

user_choice = int(input("Что вы выбирите? 0 - Камень, 1 - Бумага, 2 - Ножници !!! \n"))

if user_choice >= 3 or user_choice < 0:
    print("Не правильно указан номер")
else:
    print(images[user_choice])

computer_choice = random.randint(0, 2)
print("Компьютер выбрал:")
print(images[computer_choice])

if user_choice == 0 and computer_choice == 2:
    print("Выиграл!")
elif computer_choice == 0 and user_choice == 2:
    print("Проиграл")
elif user_choice > computer_choice:
    print("Выиграл")
elif computer_choice == user_choice:
    print("Ничья")